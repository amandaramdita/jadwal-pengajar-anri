package com.example.appsanri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mikepenz.iconics.context.IconicsLayoutInflater2;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DaftarAct extends AppCompatActivity {
    @BindView(R.id.et_full_name)
    EditText etFullName;
    @BindView(R.id.et_email_address)
    EditText etEmailAddress;
    @BindView(R.id.et_password)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daftar);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_signup)
    public void onSigUp(){
        String fullName = etFullName.getText().toString();
        String email = etEmailAddress.getText().toString();
        String password = etPassword.getText().toString();

        if (fullName.isEmpty()){
            etFullName.setError("harap diisi");
        }
        if (email.isEmpty()){
            etEmailAddress.setError("Harap diisi");
        }
        if (password.isEmpty()){
            etPassword.setError("Harap diisi");
        }
        else {
            Toast.makeText(this, "Berhasil Terdaftar", Toast.LENGTH_SHORT).show();
        }
    }

    public void Login(View view) {
        startActivity(new Intent(this, LoginAct.class));
    }


}
