package com.example.appsanri;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.appsanri.fragment.JadwalFragment;
import com.example.appsanri.fragment.KalenderFragment;
import com.example.appsanri.fragment.SettingsFragment;

public class MenuMainContent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.haldepan);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadfragment(new JadwalFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new JadwalFragment();
                loadfragment(fragment);
                return true;
            case R.id.navigation_dashboard:
                fragment = new KalenderFragment();
                loadfragment(fragment);
                return true;
            case R.id.navigation_notifications:
                fragment = new SettingsFragment();
                loadfragment(fragment);
                return true;
        }
        return false;
    };
// solat dulu lah haha
    private void loadfragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_content, fragment);
        transaction.commit();
    }

}
