package com.example.appsanri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.iconics.context.IconicsLayoutInflater2;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginAct extends AppCompatActivity {

    @BindView(R.id.username)
    EditText inputUser;
    @BindView(R.id.pass)
    EditText inputPasword;
    @BindView(R.id.forget_password)
    TextView forgetPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    String USER = "admin";
    String PASS = "admin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_login)
    public void onLoginClicked() {
        String user = inputUser.getText().toString();
        String password = inputPasword.getText().toString();
        if (user.isEmpty()) {
            inputUser.setError("harap diisi");
        }
        if (password.isEmpty()) {
            inputPasword.setError("Harap diisi");
        }
        if (user.equals(USER) && password.equals(PASS)) {
            Toast.makeText(this, "Akses Benar", Toast.LENGTH_SHORT).show();
            startActivity( new Intent(LoginAct.this, MenuMainContent.class));
            finish();
        } else {
            Toast.makeText(this, "Akses Salah", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick (R.id.sign_up)
    public void onSigupClicked(){
        startActivity(new Intent(this, DaftarAct.class));
        finish();
    }
}
