package com.example.appsanri;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splashscreen);
        new Handler().postDelayed(() -> {

            Intent i = new Intent(SplashScreen.this, WelcomeAct.class);
            startActivity(i);

            finish();
        }, SPLASH_TIME_OUT);
    }
}


