package com.example.appsanri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash2);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_login)
    public void onClick (){
        startActivity(new Intent(this, LoginAct.class));
    }

    @OnClick(R.id.btn_daftar)
    public void onRegister (){
        startActivity(new Intent(this, DaftarAct.class));
    }
}
