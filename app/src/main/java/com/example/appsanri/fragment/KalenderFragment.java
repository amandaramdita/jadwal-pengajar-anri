package com.example.appsanri.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.appsanri.R;

import butterknife.ButterKnife;

public class KalenderFragment extends Fragment {

    public KalenderFragment() {
    }

    public static KalenderFragment newInstance(String param1, String param2) {
        KalenderFragment fragment = new KalenderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
